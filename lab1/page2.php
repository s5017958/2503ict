<!DOCTYPE html> 
<html>
  <head>
    <meta charset="utf-8">
    <title>lab 1</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">
  </head>
  <body>
    <h1>Page 2</h1> 
    <ul>
        <li>Page 2 list item 1</li>
        <li>Page 2 list item 2</li>
        <li>Page 2list item 3</li>
    </ul>
    <table border="1px solid black">
        <tr>
            <th colspan="2">Page 2 Table Header</th>
        </tr>
        <tr>
            <td>Address</td>
            <td>
              <address>123 Somewhat Dr</address>
            </td>
        </tr>
    </table>
    <img src="https://i.ytimg.com/vi/Ea8rOZyawIs/hqdefault.jpg" width="250" height="250" alt="Who cares" />
    <a href="index.php">Link to home page</a>
  </body>
</html>