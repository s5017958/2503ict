<!DOCTYPE html> 
<html>
  <head>
    <meta charset="utf-8">
    <title>lab 1</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">
  </head>
  <body>
    <h1>Hello World</h1> 
    <ul>
        <li>list item 1</li>
        <li>list item 2</li>
        <li>list item 3</li>
    </ul>
    <table border="1px solid black">
        <tr>
            <th colspan="2">This is the table Head</th>
        </tr>
        <tr>
            <td>Address</td>
            <td>
              <address>77 Friday St</address>
            </td>
        </tr>
    </table>
    <img src="https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcSpAVnVVmV1cCdTHzG6SjgpocDx7ehgj05ccX_pvh4bj-GKMp8nwQ" width="250" height="250" alt="Picture" />
    <a href="page2.php">link to Page 2</a>
  </body>
</html>